#include <LiquidCrystal.h>
const int rs = 11, en = 10, d4 = 8, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

int Vmax=0;
int Vmin=1023;
int sensorValue1,sensorValue2,sensorValue;
float x,Vrms,V,SumVrms;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

    lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print(" FAS1-FAS2-FAS3");
  lcd.setCursor(15,1);
  lcd.print("V");

}

void loop() {
  for(int j=0; j<8;j++)
  {
  Vmax=0;
  Vmin=1023;
  for(int i=0; i<2000;i++)
  {
    sensorValue1=Vmax;
    sensorValue2=Vmin;
    sensorValue= analogRead(A1);
    if(sensorValue>sensorValue1)
    {
      Vmax=sensorValue;
      goto bailout;
      }
    if(sensorValue < sensorValue2) 
    {
      Vmin=sensorValue;
      goto bailout;
      }
      Vmax= sensorValue1;
      Vmin= sensorValue2;
      bailout:
      delay(0.1);
    }
    x=(Vmax-Vmin);
    V=(0.00000412*x*x*x - 0.000857*x*x  + 2.675*x - 3.198);
    Vrms=V/(2*sqrt(2));
    SumVrms=Vrms+SumVrms;
  }
  SumVrms=(SumVrms/8)-1.5;
    Serial.println(Vrms);
      lcd.setCursor(5,1);
  lcd.print(SumVrms);
  SumVrms=0;
    delay(2000);
}
