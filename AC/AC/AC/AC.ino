#include <LiquidCrystal.h>
const int rs = 11, en = 10, d4 = 8, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

int Vmax=0;
int Vmin=1023;
int sensorValue1,sensorValue2,sensorValue;
float x,Vrms,V,SumVrms,factor;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

    lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("  Voltimetro  ");
  lcd.setCursor(0,1);
  lcd.print("  trifasico   ");
  delay(1500);
  lcd.setCursor(0,0);
  lcd.print("                 ");
  lcd.setCursor(0,1);
  lcd.print("                 ");
  
  lcd.setCursor(0,1);          
  lcd.print("F3=");  

  lcd.setCursor(0,0);
  lcd.print("F1=");  
factor=2*sqrt(2);
}

void loop() 
{
//fase1();
//fase2();
fase3();
}



    void fase1()
    {
      Vmax=0;
      Vmin=0;
      sensorValue1=0.0;
      sensorValue2=0.0;
      sensorValue=0.0;
      V=0;
      Vrms=0;
      SumVrms=0;
      x=0.0;
  for(int j=0; j<15;j++)
  {
  Vmax=0;
  Vmin=1023;
  for(int i=0; i<1000;i++)
  {
    sensorValue1=Vmax;
    sensorValue2=Vmin;
    sensorValue= analogRead(A1);
    if(sensorValue>sensorValue1)
    {
      Vmax=sensorValue;
      goto bailout;
      }
    if(sensorValue < sensorValue2) 
    {
      Vmin=sensorValue;
      goto bailout;
      }
      Vmax= sensorValue1;
      Vmin= sensorValue2;
      bailout:
      delay(0.001);
    }
    x=(Vmax-Vmin);
    V=(0.00000412*x*x*x - 0.000857*x*x  + 2.675*x - 3.198);
    Vrms=Vrms+V;
    
  }
     SumVrms=Vrms/15;
  SumVrms=(SumVrms/factor)-7;
  if (SumVrms<-0.1)
  {SumVrms=0.0000;}    

      
      lcd.setCursor(3,0);
  lcd.print(SumVrms);
  SumVrms=0;
    delay(500);      
      }

void fase2()
{
    for(int j=0; j<4;j++)
  {
  Vmax=0;
  Vmin=1023;
  for(int i=0; i<1000;i++)
  {
    sensorValue1=Vmax;
    sensorValue2=Vmin;
    sensorValue= analogRead(A0);
    if(sensorValue>sensorValue1)
    {
      Vmax=sensorValue;
      goto bailout;
      }
    if(sensorValue < sensorValue2) 
    {
      Vmin=sensorValue;
      goto bailout;
      }
      Vmax= sensorValue1;
      Vmin= sensorValue2;
      bailout:
      delay(0.001);
    }
    x=(Vmax-Vmin);
    V=(0.00000412*x*x*x - 0.000857*x*x  + 2.675*x - 3.198);
    Vrms=V/(2*sqrt(2))-100.5;
    SumVrms=Vrms+SumVrms;
  }
  SumVrms=(SumVrms/4);
  if (SumVrms<-0.1)
  {SumVrms=0.0000;}    
      lcd.setCursor(9,0);
  lcd.print("  F2=");
      lcd.setCursor(10,1);
  lcd.print(SumVrms);
  SumVrms=0;
    delay(500);
  }      



  void fase3()
  {
 /*     Vmax=0;
      Vmin=0;
      sensorValue1=0.0;
      sensorValue2=0.0;
      sensorValue=0.0;
      V=0;
      Vrms=0;
      SumVrms=0;
      x=0.0;*/
  for(int j=0; j<15;j++)
  {
  Vmax=0;
  Vmin=1023;
  for(int i=0; i<1000;i++)
  {
    sensorValue1=Vmax;
    sensorValue2=Vmin;
    sensorValue= analogRead(A2);
    if(sensorValue>sensorValue1)
    {
      Vmax=sensorValue;
      goto bailout;
      }
    if(sensorValue < sensorValue2) 
    {
      Vmin=sensorValue;
      goto bailout;
      }
      Vmax= sensorValue1;
      Vmin= sensorValue2;
      bailout:
      delay(0.001);
    }
    x=(Vmax-Vmin);
    V=(0.00000412*x*x*x - 0.000857*x*x  + 2.675*x - 3.198);
    Vrms=Vrms+V;
   
  }
   SumVrms=Vrms/15;
  SumVrms=(SumVrms/factor)-7;
  if (SumVrms<-0.1)
  {SumVrms=0.0000;}
    
    

  lcd.setCursor(3,1);
  lcd.print(SumVrms);
  Vrms=0;
    delay(500);    
    }  
