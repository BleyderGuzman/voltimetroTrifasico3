#include <LiquidCrystal.h>
const int rs = 11, en = 10, d4 = 8, d5 = 6, d6 = 5, d7 = 4;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

int Vmax=0;
int Vmin=1023;
int sensorValue1,sensorValue2,sensorValue;
float x,Vrms,V,SumVrms;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

    lcd.begin(16, 2);
  // Print a message to the LCD.
  
  
  lcd.setCursor(0,0);
  lcd.print("F1=");
    lcd.setCursor(9,0);
  lcd.print("F2=");
    lcd.setCursor(0,1);
  lcd.print("F3=");
}

void loop() 
{
fase1();
fase2();
fase3();
}

    void fase2()
    {
      float result=0.00;
  for(int j=0; j<8;j++)
  {
  Vmax=0;
  Vmin=1023;
  for(int i=0; i<700;i++)
  {
    sensorValue1=Vmax;
    sensorValue2=Vmin;
    sensorValue= analogRead(A0);
    if(sensorValue>sensorValue1)
    {
      Vmax=sensorValue;
      goto bailout;
      }
    if(sensorValue < sensorValue2) 
    {
      Vmin=sensorValue;
      goto bailout;
      }
      Vmax= sensorValue1;
      Vmin= sensorValue2;
      bailout:
          delayMicroseconds(83);  // para completar un ciclo completo 
    }
    x=(Vmax-Vmin);
    //V=(0.00000412*x*x*x - 0.000857*x*x  + 2.675*x - 3.198);
    Vrms=x/(sqrt(2));
    //Serial.println(x);
    
    SumVrms=Vrms+SumVrms;
  }
  SumVrms=(SumVrms/8);
    result= SumVrms;
      lcd.setCursor(11,1);
  lcd.print(result);
  SumVrms=0;
    //delay(2000);    
      }
    
    
    void fase3()
    {
      float result=0.00;
        for(int j=0; j<8;j++)
          {
              Vmax=0;
              Vmin=1023;
            for(int i=0; i<700;i++)
              {
                sensorValue1=Vmax;
                 sensorValue2=Vmin;
                 sensorValue= analogRead(A2);
              if(sensorValue>sensorValue1)
                {
                  Vmax=sensorValue;
                  goto bailout;
                }
              if(sensorValue < sensorValue2) 
                  {
                    Vmin=sensorValue;
                    goto bailout;
                  }
                      Vmax= sensorValue1;
                      Vmin= sensorValue2;
      bailout:
                    delayMicroseconds(83);  // para completar un ciclo completo 
    }
    x=(Vmax-Vmin);
    //V=(0.00000412*x*x*x - 0.000857*x*x  + 2.675*x - 3.198);
    Vrms=x/(sqrt(2));
    //Serial.println(x);
    
    SumVrms=Vrms+SumVrms;
  }
  SumVrms=(SumVrms/8);
    
    result=SumVrms;
      lcd.setCursor(3,1);
  lcd.print(result);
  SumVrms=0;
    //delay(2000);    
      }

    
    void fase1()
    {
      float result=0.00;
  for(int j=0; j<8;j++)
  {
  Vmax=0;
  Vmin=1023;
  for(int i=0; i<700;i++)
  {
    sensorValue1=Vmax;
    sensorValue2=Vmin;
    sensorValue= analogRead(A1);
    if(sensorValue>sensorValue1)
    {
      Vmax=sensorValue;
      goto bailout;
      }
    if(sensorValue < sensorValue2) 
    {
      Vmin=sensorValue;
      goto bailout;
      }
      Vmax= sensorValue1;
      Vmin= sensorValue2;
      bailout:
          delayMicroseconds(83);  // para completar un ciclo completo 
    }
    x=(Vmax-Vmin);
    //V=(0.00000412*x*x*x - 0.000857*x*x  + 2.675*x - 3.198);
    Vrms=x/(sqrt(2));
    //Serial.println(x);
    
    SumVrms=Vrms+SumVrms;
  }
  SumVrms=(SumVrms/8);
    
      result=SumVrms;
      lcd.setCursor(3,0);
  
  lcd.print(result);
  SumVrms=0;
    //delay(2000);    
      }
